﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public enum ResourceTypes{ Ice }
    public ResourceTypes resourceType;

    public float harvestTime;
    public float availableResource;

    public int gatherers;

    void Start()
    {
        StartCoroutine(ResourceTick());
    }

    void Update()
    {
        if(availableResource <= 0)
        {
            Destroy(gameObject);
        }
    }

    public void ResourceGather()
    {
        if(gatherers != 0)
        {
            availableResource -= gatherers;

        }
    }
    IEnumerator ResourceTick()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
            ResourceGather();
        }
    }
}
