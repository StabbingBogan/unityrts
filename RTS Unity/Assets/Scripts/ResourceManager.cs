﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public float ice;
    public float maxIce;
    public float iron;
    public float maxIron;
    public float food;
    public float maxFood;
    public float oxygen;
    public float maxOxygen;
    public float population;
    public float maxPopulation;

    public Text iceDisp;
    public Text iceIron;
    public Text iceFood;
    public Text iceOxygen;
    public Text icePopulation;

    void Start()
    {
        
    }

    void Update()
    {
        iceDisp.text = "" + ice + "/" + maxIce;
        iceDisp.text = "" + maxIron + "/" + maxIron;
        iceDisp.text = "" + food + "/" + maxFood;
        iceDisp.text = "" + oxygen + "/" + maxOxygen;
        iceDisp.text = "" + iceOxygen + "/" + icePopulation;

    }
}
